﻿using System;
using DG.Tweening;
using General;
using Player;
using UnityEngine;

namespace Traps
{
    [RequireComponent(typeof(BoxCollider))]
    public class Trap : MonoBehaviour
    {
        [SerializeField] private float _riseDelay = 1.4f;
        [SerializeField] private float _riseDuration = 0.1f;
        [SerializeField] private TrapDeactivator _deactivator;

        private float _yScale;
        private Sequence _animation;

        public event Action Moved;
        
        private void Start()
        {
            LevelFlow.Instance.Started += OnStarted;
            _deactivator.Deactivated += OnDeactivated;
            
            _yScale = transform.localScale.y;
        }

        private void OnStarted()
        {
            LevelFlow.Instance.Started -= OnStarted;
            
            Activate();
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(out PlayerCollider _))
            {
                Deactivate();
            }
        }

        private void Activate()
        {
            _animation = DOTween.Sequence()
                .PrependInterval(_riseDelay)
                .AppendCallback(()=>Moved?.Invoke())
                .Join(transform.DOScale(new Vector3(1f, 0f, 1f), _riseDuration))
                .AppendInterval(_riseDelay)
                .AppendCallback(()=>Moved?.Invoke())
                .Join(transform.DOScale(new Vector3(1f, _yScale, 1f), _riseDuration))
                .SetLoops(-1);
        }

        private void Deactivate()
        {
            _animation.Kill();
            
            transform.localScale = new Vector3(1f, 0f, 1f);
        }

        private void OnDeactivated()
        {
            _deactivator.Deactivated -= OnDeactivated;
            Deactivate();
        }
    }
}