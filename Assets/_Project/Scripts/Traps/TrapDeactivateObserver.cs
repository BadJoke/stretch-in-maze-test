﻿using System;
using UnityEngine;

namespace Traps
{
    public class TrapDeactivateObserver : MonoBehaviour
    {
        [SerializeField] private TrapDeactivator[] _deactivators;

        public event Action TrapDeactivated;

        private void OnEnable()
        {
            foreach (TrapDeactivator deactivator in _deactivators)
            {
                deactivator.Deactivated += OnTrapDeactivated;
            }
        }

        private void OnDisable()
        {
            foreach (TrapDeactivator deactivator in _deactivators)
            {
                deactivator.Deactivated -= OnTrapDeactivated;
            }
        }

        private void OnTrapDeactivated()
        {
            TrapDeactivated?.Invoke();
        }
    }
}