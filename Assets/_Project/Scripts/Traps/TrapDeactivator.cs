﻿using System;
using Player;
using UnityEngine;

namespace Traps
{
    [RequireComponent(typeof(Collider))]
    public class TrapDeactivator : MonoBehaviour
    {
        private Collider _trigger;
        
        public event Action Deactivated;

        private void Start()
        {
            _trigger = GetComponent<Collider>();
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(out PlayerCollider _))
            {
                _trigger.enabled = false;
                Deactivated?.Invoke();
            }
        }
    }
}