﻿using General;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ScorePresenter : MonoBehaviour
    {
        [SerializeField] private Score _score;
        
        private TextMeshProUGUI _scoreField;

        private void Awake()
        {
            _scoreField = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable()
        {
            _score.AmountChanged += OnAmountChanged;
        }

        private void OnDisable()
        {
            _score.AmountChanged -= OnAmountChanged;
        }

        private void OnAmountChanged(uint amount)
        {
            _scoreField.text = amount.ToString();
        }
    }
}