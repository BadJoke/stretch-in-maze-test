﻿using System;
using DG.Tweening;
using UnityEngine;

namespace UI.Finish
{
    public partial class FinishPanel
    {
        [Serializable]
        public class FinishResultObject
        {
            [SerializeField] private Vector3 _smileJellySize = Vector3.one;

            public void Show(RectTransform rectTransform, float duration)
            {
                DOTween.Sequence()
                    .Append(rectTransform.DOScale(_smileJellySize, duration * 0.8f))
                    .Append(rectTransform.DOScale(1f, duration * 0.2f));
            }
        }
    }
}