﻿using DG.Tweening;
using General;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Finish
{
    public partial class FinishPanel : MonoBehaviour
    {
        [SerializeField] private float _animationDuration = 0.5f;
        [SerializeField] private Image _background;
        [SerializeField] private FinishTitle _title;
        [SerializeField] private FinishResultObject _finishResultObject;
        [SerializeField] private FinishButton _button;
        [SerializeField] private FinishData _failedData;
        [SerializeField] private FinishData _passedData;

        private LevelFlow _levelFlow;
        
        private void Start()
        {
            _levelFlow = LevelFlow.Instance;
            _levelFlow.Failed += OnFailed;
            _levelFlow.Passed += OnPassed;
        }

        private void Finish()
        {
            _levelFlow.Failed -= OnFailed;
            _levelFlow.Passed -= OnPassed;
        }

        private void Show(FinishData data)
        {
            _background.DOFade(data.BackgroundAlpha, _animationDuration);
            _title.Show(data.Message, _animationDuration);
            _finishResultObject.Show(data.FinishObject, _animationDuration);
            _button.Show(data.ButtonColor, data.ButtonText, _animationDuration);
        }

        private void OnFailed()
        {
            Finish();
            Show(_failedData);
        }

        private void OnPassed()
        {
            Finish();
            Show(_passedData);
        }
    }
}