﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace UI.Finish
{
    public partial class FinishPanel
    {
        [Serializable]
        public class FinishTitle
        {
            [SerializeField] private float _yPosition = -340f;
            [SerializeField] private TextMeshProUGUI _text;

            public void Show(string message, float duration)
            {
                _text.text = message;
                _text.rectTransform.DOAnchorPosY(_yPosition, duration);
            }
        }
    }
}