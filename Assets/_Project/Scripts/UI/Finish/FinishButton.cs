﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Finish
{
    public partial class FinishPanel
    {
        [Serializable]
        public class FinishButton
        {
            [SerializeField] private Vector3 _buttonJellySize = Vector3.one;
            [SerializeField] private RectTransform _button;
            [SerializeField] private Image _buttonSprite;
            [SerializeField] private TextMeshProUGUI _buttonText;

            public void Show(Color color, string text, float duration)
            {
                _buttonSprite.color = color;
                _buttonText.text = text;

                DOTween.Sequence()
                    .Append(_button.DOScale(_buttonJellySize, duration * 0.8f))
                    .Append(_button.DOScale(1f, duration * 0.2f));
            }
        }
    }
}