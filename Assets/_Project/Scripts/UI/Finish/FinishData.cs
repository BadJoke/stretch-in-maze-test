﻿using System;
using UnityEngine;

namespace UI.Finish
{
    public partial class FinishPanel
    {
        [Serializable]
        public class FinishData
        {
            [SerializeField, Range(0f, 1f)] private float _backgroundAlpha = 0.5f;
            [SerializeField] private string _message;
            [SerializeField] private string _buttonText;
            [SerializeField] private Color _buttonColor = Color.white;
            [SerializeField] private RectTransform _finishObject;

            public float BackgroundAlpha => _backgroundAlpha;
            public string Message => _message;
            public string ButtonText => _buttonText;
            public Color ButtonColor => _buttonColor;
            public RectTransform FinishObject => _finishObject;
        }
    }
}