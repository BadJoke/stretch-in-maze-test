﻿using DG.Tweening;
using UnityEngine;

namespace UI
{
    public class TapAnimation : MonoBehaviour
    {
        [SerializeField] private float _duration = 2f;
        [SerializeField] private float _size = 1.5f;

        private void Start()
        {
            DOTween.Sequence()
                .Append(transform.DOScale(_size, _duration * 0.5f))
                .Append(transform.DOScale(1f, _duration * 0.5f))
                .SetLoops(-1);
        }
    }
}