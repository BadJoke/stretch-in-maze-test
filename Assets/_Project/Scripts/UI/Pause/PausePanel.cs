﻿using DG.Tweening;
using General;
using UnityEngine;

namespace UI.Pause
{
    public class PausePanel : MonoBehaviour
    {
        [SerializeField] private float _jellyScale = 1.4f;

        private bool _isPaused;
        private LevelFlow _levelFlow;
        
        private void Start()
        {
            _isPaused = false;
            
            _levelFlow = LevelFlow.Instance;
            _levelFlow.Paused += OnPaused;
            _levelFlow.Resumed += OnResumed;
            _levelFlow.Passed += OnFinished;
            _levelFlow.Failed += OnFinished;
        }

        private void OnPaused()
        {
            _isPaused = true;
            
            DOTween.Sequence()
                .Append(transform.DOScale(_jellyScale, 0.4f))
                .Append(transform.DOScale(1f, 0.1f));
        }

        private void OnResumed()
        {
            _isPaused = false;
            
            DOTween.Sequence()
                .Append(transform.DOScale(_jellyScale, 0.1f))
                .Append(transform.DOScale(0f, 0.4f));
        }
        
        private void OnFinished()
        {
            _levelFlow.Paused -= OnPaused;
            _levelFlow.Resumed -= OnResumed;
            _levelFlow.Passed -= OnFinished;
            _levelFlow.Failed -= OnFinished;

            if (_isPaused)
            {
                OnResumed();
            }
        }
    }
}