﻿using DG.Tweening;
using General;
using UnityEngine;

namespace UI.Pause
{
    public class PauseButtonPresenter : MonoBehaviour
    {
        private LevelFlow _levelFlow;
        
        private void Start()
        {
            _levelFlow = LevelFlow.Instance;
            _levelFlow.Started += OnStarted;
            _levelFlow.Failed += OnPassed;
            _levelFlow.Passed += OnPassed;
        }

        private void OnStarted()
        {
            _levelFlow.Started -= OnStarted;
            
            DOTween.Sequence()
                .Append(transform.DOScale(1.4f, 0.3f))
                .Append(transform.DOScale(1f, 0.1f));
        }

        private void OnPassed()
        {
            _levelFlow.Failed -= OnPassed;
            _levelFlow.Passed -= OnPassed;
            
            DOTween.Sequence()
                .Append(transform.DOScale(1.4f, 0.1f))
                .Append(transform.DOScale(0f, 0.3f));
        }
    }
}