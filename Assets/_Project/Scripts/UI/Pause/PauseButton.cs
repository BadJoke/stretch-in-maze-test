﻿using System;
using UnityEngine;

namespace UI.Pause
{
    [RequireComponent(typeof(PauseButtonPresenter))]
    public class PauseButton : MonoBehaviour
    {
        private bool _isPaused;

        public event Action Paused;
        public event Action Resumed;

        private void Start()
        {
            _isPaused = false;
        }

        public void OnClick()
        {
            if (_isPaused)
            {
                _isPaused = false;
                Resumed?.Invoke();
            }
            else
            {
                _isPaused = true;
                Paused?.Invoke();
            }
        }
    }
}