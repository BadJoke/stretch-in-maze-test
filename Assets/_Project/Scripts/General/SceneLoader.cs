﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace General
{
    public class SceneLoader : MonoBehaviour
    {
        private int _currentLevel;
        private bool _isLevelFailed;
        
        private void Start()
        {
            _currentLevel = SceneManager.GetActiveScene().buildIndex;
            _isLevelFailed = true;
            
            LevelFlow.Instance.Passed += OnPassed;
        }

        public void Load()
        {
            if (_isLevelFailed)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            else
            {
                int nextLevelIndex = ++_currentLevel % SceneManager.sceneCount;
                
                SceneManager.LoadScene(nextLevelIndex);
            }
        }

        private void OnPassed()
        {
            LevelFlow.Instance.Passed -= OnPassed;
            
            _isLevelFailed = false;
        }
    }
}