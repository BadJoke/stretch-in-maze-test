﻿using System;
using Traps;
using UnityEngine;

namespace General
{
    public class Score : MonoBehaviour
    {
        [SerializeField] private uint _scoreByTrap = 50;
        [SerializeField] private uint _scoreByLevel = 250;
        [SerializeField] private TrapDeactivateObserver _trapDeactivateObserver;

        private uint _amount = 0;
        
        public event Action<uint> AmountChanged;

        private void OnEnable()
        {
            _trapDeactivateObserver.TrapDeactivated += OnTrapDeactivated;
        }

        private void OnDisable()
        {
            _trapDeactivateObserver.TrapDeactivated -= OnTrapDeactivated;
        }

        private void Start()
        {
            LevelFlow.Instance.Passed += OnLevelPassed;
        }

        private void AddScore(uint value)
        {
            _amount += value;
            AmountChanged?.Invoke(_amount);
        }

        private void OnTrapDeactivated()
        {
            AddScore(_scoreByTrap);
        }

        private void OnLevelPassed()
        {
            AddScore(_scoreByLevel);
        }
    }
}