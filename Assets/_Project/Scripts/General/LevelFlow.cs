﻿using System;
using Player;
using UI.Pause;
using UnityEngine;

namespace General
{
    public class LevelFlow : MonoBehaviour
    {
        [SerializeField] private PauseButton _pauseButton;
        [SerializeField] private Figure _figure;
        [SerializeField] private PlayerCollider _playerCollider;
        
        public event Action Started;
        public event Action Paused;
        public event Action Resumed;
        public event Action Failed;
        public event Action Passed;

        public static LevelFlow Instance { get; private set; }

        private void Awake()
        {
            Instance = this;

            Subscribe();
        }

        private void Subscribe()
        {
            _pauseButton.Paused += OnPaused;
            _pauseButton.Resumed += OnResumed;
            _playerCollider.Died += OnDied;
            _figure.Finished += OnFinished;
        }

        private void Unsubscribe()
        {
            _pauseButton.Paused -= OnPaused;
            _pauseButton.Resumed -= OnResumed;
            _playerCollider.Died -= OnDied;
            _figure.Finished -= OnFinished;
        }

        public void OnStartClicked()
        {
            Started?.Invoke();
        }

        private void OnPaused()
        {
            Paused?.Invoke();
        }

        private void OnResumed()
        {
            Resumed?.Invoke();
        }

        private void OnDied()
        {
            Unsubscribe();
            
            Failed?.Invoke();
        }
        
        private void OnFinished()
        {
            Unsubscribe();

            Passed?.Invoke();
        }
    }
}