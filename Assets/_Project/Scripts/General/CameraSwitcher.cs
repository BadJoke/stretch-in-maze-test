﻿using Cinemachine;
using UnityEngine;

namespace General
{
    public class CameraSwitcher : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _camera;

        private void Start()
        {
            LevelFlow.Instance.Started += OnStarted;
        }

        private void OnStarted()
        {
            LevelFlow.Instance.Started -= OnStarted;
            _camera.gameObject.SetActive(true);
            Destroy(this);
        }
    }
}