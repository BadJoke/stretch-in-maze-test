﻿using General;
using Traps;
using UnityEngine;

namespace Sounds
{
    [RequireComponent(typeof(AudioSource))]
    public class EventSounds : MonoBehaviour
    {
        [SerializeField] private TrapDeactivateObserver _trapDeactivateObserver;
        [SerializeField] private AudioClip _trapDeactivated;
        [SerializeField] private AudioClip _passed;
        [SerializeField] private AudioClip _failed;

        private AudioSource _source;
        private LevelFlow _levelFlow;
        
        private void Start()
        {
            _source = GetComponent<AudioSource>();
            _levelFlow = LevelFlow.Instance;

            Subscribe();
        }

        private void Subscribe()
        {
            _trapDeactivateObserver.TrapDeactivated += OnTrapDeactivated;
            _levelFlow.Failed += OnFailed;
            _levelFlow.Passed += OnPassed;
        }

        private void Unsubscribe()
        {
            _trapDeactivateObserver.TrapDeactivated -= OnTrapDeactivated;
            _levelFlow.Failed -= OnFailed;
            _levelFlow.Passed -= OnPassed;
        }

        private void PlayClip(AudioClip clip)
        {
            _source.clip = clip;
            _source.Play();
        }

        private void OnTrapDeactivated()
        {
            PlayClip(_trapDeactivated);
        }

        private void OnFailed()
        {
            Unsubscribe();
            PlayClip(_failed);
        }

        private void OnPassed()
        {
            Unsubscribe();
            PlayClip(_passed);
        }
    }
}