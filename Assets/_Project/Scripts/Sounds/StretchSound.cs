﻿using Player;
using UnityEngine;

namespace Sounds
{
    [RequireComponent(typeof(AudioSource))]
    public class StretchSound : MonoBehaviour
    {
        [SerializeField] private InputController _inputController;
        
        private AudioSource _source;

        private void OnEnable()
        {
            _inputController.HoldPerformed += OnHoldPerformed;
            _inputController.HoldCanceled += OnHoldCanceled;
        }

        private void OnDisable()
        {
            _inputController.HoldPerformed -= OnHoldPerformed;
            _inputController.HoldCanceled -= OnHoldCanceled;
        }

        private void Start()
        {
            _source = GetComponent<AudioSource>();
        }

        private void OnHoldPerformed()
        {
            _source.Play();
        }

        private void OnHoldCanceled()
        {
            _source.Stop();
        }
    }
}