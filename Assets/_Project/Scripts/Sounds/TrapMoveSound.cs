﻿using Traps;
using UnityEngine;

namespace Sounds
{
    [RequireComponent(typeof(AudioSource))]
    public class TrapMoveSound : MonoBehaviour
    {
        [SerializeField] private Trap _trap;
        
        private AudioSource _source;

        private void OnEnable()
        {
            _trap.Moved += OnMoved;
        }

        private void OnDisable()
        {
            _trap.Moved -= OnMoved;
        }

        private void Start()
        {
            _source = GetComponent<AudioSource>();
        }

        private void OnMoved()
        {
            _source.Play();
        }
    }
}