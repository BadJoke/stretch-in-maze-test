﻿using System.Collections.Generic;
using SplineMesh;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Spline))]
    [RequireComponent(typeof(MeshBender))]
    [RequireComponent(typeof(MeshRenderer))]
    public class GeneratedMesh : MonoBehaviour
    {
        [SerializeField] private Material _material;
        [SerializeField] private Mesh _mesh;
        [SerializeField] private Vector3 _rotation = new Vector3(0f, 90f, 0f);

        private Spline _spline;
        private MeshBender _meshBender;
        private MeshRenderer _meshRenderer;

        public float SplineLength => _spline.Length;
        
        public void Init(CubicBezierCurve curve)
        {
            _spline = GetComponent<Spline>();
            _spline.nodes = new List<SplineNode>{curve.n1, curve.n2};
            _spline.RefreshCurves();
            
            _meshRenderer = GetComponent<MeshRenderer>();
            _meshRenderer.material = _material;

            _meshBender = GetComponent<MeshBender>();
            _meshBender.Source = SourceMesh.Build(_mesh)
                .Rotate(Quaternion.Euler(_rotation))
                .Scale(Vector3.one);
            _meshBender.Mode = MeshBender.FillingMode.StretchToInterval;
            _meshBender.SetInterval(_spline, 0f);
            
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void SetLength(float length)
        {
            float finalLength = length < _spline.Length ? length : _spline.Length;
            
            _meshBender.SetInterval(_spline, 0f, finalLength);
            _meshBender.ComputeIfNeeded();
        }
    }
}