﻿using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ConfettiInvoker : MonoBehaviour
    {
        [SerializeField] private Figure _figure;

        private ParticleSystem _confetti;

        private void Start()
        {
            _confetti = GetComponent<ParticleSystem>();
            _figure.Finished += OnFinished;
        }

        private void OnFinished()
        {
            _figure.Finished -= OnFinished;
            _confetti.Play();
        }
    }
}