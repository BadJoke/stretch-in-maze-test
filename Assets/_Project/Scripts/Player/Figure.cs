﻿using System;
using System.Collections.Generic;
using SplineMesh;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Spline))]
    public class Figure : MonoBehaviour
    {
        [SerializeField] private Spline _spline;
        [SerializeField] private float _startScale = 1f;
        [SerializeField] private float _endScale = 0f;
        [SerializeField] private float _moveSpeed = 1f;
        [SerializeField] private GeneratedMesh _generatedMeshPrefab;

        private float _rate;
        private int _meshIndex;
        private float _passedLength;
        private List<CubicBezierCurve> _curves;
        private GeneratedMesh[] _generatedMeshes;
        
        public event Action<Vector3> PositionChanged;
        public event Action Finished;

        public bool IsNotFinished { get; private set; }

        private void Start()
        {
            IsNotFinished = true;
            
            _rate = 0f;
            _meshIndex = 0;
            _passedLength = 0f;
            _curves = _spline.curves;

            _generatedMeshes = new GeneratedMesh[_curves.Count];

            for (int i = 0; i < _curves.Count; i++)
            {
                _generatedMeshes[i] = Instantiate(_generatedMeshPrefab, transform);
                _generatedMeshes[i].Init(_curves[i]);
            }
            
            _generatedMeshes[_meshIndex].Show();
        }

        public void Tick()
        {
            float step = Time.deltaTime / (_spline.Length / _moveSpeed);
            
            _rate += step;
            _passedLength += _spline.Length * step;
            
            if (_rate > 1f)
            {
                IsNotFinished = false;
                
                Finished?.Invoke();
                
                return;
            }

            if (_passedLength > _generatedMeshes[_meshIndex].SplineLength && _meshIndex < _generatedMeshes.Length - 1)
            {
                _passedLength -= _generatedMeshes[_meshIndex].SplineLength;
                _generatedMeshes[_meshIndex].SetLength(_generatedMeshes[_meshIndex].SplineLength);
                _meshIndex++;
                _generatedMeshes[_meshIndex].Show();
            }

            UpdatePosition();
            Contort();
        }

        private void UpdatePosition()
        {
            CurveSample sample = _spline.GetSampleAtDistance(_spline.Length * _rate);
            Vector3 position = transform.TransformPoint(sample.location);
            
            PositionChanged?.Invoke(position);
        }

        private void Contort() 
        {
            float nodeDistance = 0;
            
            for (int i = 0; i < _curves.Count && nodeDistance <= _spline.Length * _rate; i++)
            {
                _spline.nodes[i].Scale = GetNodeSize(nodeDistance);
                nodeDistance += _spline.curves[i].Length;
                _spline.nodes[i + 1].Scale = GetNodeSize(nodeDistance);
            }
            
            _generatedMeshes[_meshIndex].SetLength(_passedLength);
        }

        private Vector2 GetNodeSize(float nodeDistance)
        {
            float nodeDistanceRate = nodeDistance / _spline.Length;
            float nodeScale = Mathf.Lerp(_endScale, _startScale, _rate - nodeDistanceRate);

            return new Vector2(nodeScale, nodeScale);
        }
    }
}