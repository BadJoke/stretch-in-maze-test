﻿using System.Collections;
using General;
using UnityEngine;

namespace Player
{
    public class MoveController : MonoBehaviour
    {
        [SerializeField] private InputController _inputController;
        [SerializeField] private Figure _figure;

        private LevelFlow _levelFlow;
        private Coroutine _holding;

        private void Start()
        {
            _levelFlow = LevelFlow.Instance;
            _levelFlow.Failed += Stop;
            _levelFlow.Passed += Stop;
            
            _inputController.HoldPerformed += OnHoldPerformed;
            _inputController.HoldCanceled += OnHoldCanceled;
        }

        private IEnumerator Holding()
        {
            while (_figure.IsNotFinished)
            {
                _figure.Tick();
                
                yield return null;
            }
        }

        private void Stop()
        {
            _inputController.HoldPerformed -= OnHoldPerformed;
            _inputController.HoldCanceled -= OnHoldCanceled;
            
            _levelFlow.Failed -= Stop;
            _levelFlow.Passed -= Stop;
            
            StopCoroutine(_holding);
        }

        private void OnHoldPerformed()
        {
            _holding = StartCoroutine(Holding());
        }
        
        private void OnHoldCanceled()
        {
            if (_holding != null)
            {
                StopCoroutine(_holding);
            }
        }
    }
}