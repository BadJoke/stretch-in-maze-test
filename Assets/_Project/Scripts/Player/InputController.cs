﻿using System;
using General;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class InputController : MonoBehaviour
    {
        private PlayerControls _playerControls;
        private InputAction _holdAction;
        private LevelFlow _levelFlow;
        
        public event Action HoldPerformed;
        public event Action HoldCanceled;
        
        private void Awake()
        {
            _playerControls = new PlayerControls();
            
            _holdAction = _playerControls.MobileMap.Hold;
            _holdAction.performed += OnHoldPerformed;
            _holdAction.canceled += OnHoldCanceled;
        }

        private void OnEnable()
        {
            _playerControls.Enable();
        }

        private void OnDisable()
        {
            _playerControls.Disable();
        }

        private void Start()
        {
            _levelFlow =  LevelFlow.Instance;
            _levelFlow.Started += OnStarted;
            _levelFlow.Paused += OnPaused;
            _levelFlow.Resumed += OnResumed;
            _levelFlow.Failed += OnLevelFinished;
            _levelFlow.Passed += OnLevelFinished;
            
            _playerControls.Disable();
        }

        private void OnStarted()
        {
            _levelFlow.Started -= OnStarted;
            
            _playerControls.Enable();
        }

        private void OnPaused()
        {
            enabled = false;
        }
        
        private void OnResumed()
        {
            enabled = true;
        }

        private void OnLevelFinished()
        {
            _levelFlow.Paused -= OnPaused;
            _levelFlow.Resumed -= OnResumed;
            _levelFlow.Failed -= OnLevelFinished;
            _levelFlow.Passed -= OnLevelFinished;
            
            _playerControls.Disable();
        }

        private void OnHoldCanceled(InputAction.CallbackContext ctx)
        {
            HoldCanceled?.Invoke();
        }

        private void OnHoldPerformed(InputAction.CallbackContext ctx)
        {
            HoldPerformed?.Invoke();
        }
    }
}