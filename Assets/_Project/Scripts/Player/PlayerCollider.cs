﻿using System;
using Traps;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(SphereCollider))]
    public class PlayerCollider : MonoBehaviour
    {
        [SerializeField] private Figure _figure;

        public event Action Died;
        
        private void OnEnable()
        {
            _figure.PositionChanged += OnPositionChanged;
        }

        private void OnDisable()
        {
            _figure.PositionChanged -= OnPositionChanged;
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Trap _))
            {
                Died?.Invoke();
            }
        }

        private void OnPositionChanged(Vector3 position)
        {
            transform.Translate(position - transform.position);
        }
    }
}